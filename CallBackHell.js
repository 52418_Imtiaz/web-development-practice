// EXample of CALLBACK HELL

let  store={    // created an Object for the Icrecream Store

    fruits: ['Strawberry','Mango','Chiku','Cherry'],
    liquids: ['water','ice'],
    holdings: ['cone','cup','stick'],
    toppings:['Choco Chips','Dry Fruits'],
};


let order=(fruitName,callToProduction)=>{
    console.log("WELCOME TO ICE CREAM PARLOUR");
    setTimeout(()=>{
        console.log(`${store.fruits[fruitName]} has been Selected`);
    
        callToProduction(0,1);  // CallBack Function
    },2000);
    
};
let production=(holdings,toppings)=>{
    setTimeout(()=>{
        console.log('production has started');
        setTimeout(()=>{
            console.log('Fruits have been Chopped');
            setTimeout(()=>{
                console.log(`${store.liquids[1]}is selected`);
            },1000);
            setTimeout(()=>{
                console.log('Machine has Started');
                setTimeout(()=>{
                    console.log(`Ice Cream is Placed on ${store.holdings[holdings]}`);
                    setTimeout(()=>{
                        console.log(`${store.toppings[toppings]}have been added`);
                        setTimeout(()=>{
                            console.log(`Serve the Ice Cream`);
                            setTimeout(()=>{
                                console.log(`Store is Closing Thank You for buying`);
                            },1000)
                        },1000)
                    },2000)
                },1000)
            },1000)
        },2000)
    },0)
}
order(1,production);