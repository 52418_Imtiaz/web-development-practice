import java.util.*;
class bsb {
    
    public static void main(String[]args){
        int arr[] = {0,1,1,0,1};
        int ans= 0;

        HashMap<Integer,Integer>map=new HashMap<>();
        map.put(0,1);
        int sum=0;

        for(int val:arr){

            if(val==0){
                sum+=-1;
            }else{
                sum+= +1;
            }
            if(map.containsKey(sum)){
                ans+=map.get(sum);
                map.put(sum,map.get(sum)+1);
            }else{
                map.put(sum,1);
            }

        }
        System.out.println(ans);
    }
}
